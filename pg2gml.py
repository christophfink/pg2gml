#!/usr/bin/python -u
# -*- coding: utf-8 -*-
#
#  pg2gml.py – cgi serves postgis layers as gml 
#  
#  Copyright 2012 Christoph Fink <christoph@localhost.localdomain>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

from __future__ import print_function
import os,sys,cgi,cgitb,urllib2
cgitb.enable()

from osgeo import ogr,osr
from tempfile import mkstemp

from pg_conn import pg_conn,table

def main():
	ds=ogr.Open("PG:"+pg_conn)
	if ds is None:
		return
	for layer in ds: 
		if layer.GetName()==table:
			tmpfile=mkstemp(suffix=".gml")
			tmp=ogr.GetDriverByName("GML").CreateDataSource(tmpfile[1])
			if tmp is not None:
				tmp.CopyLayer(layer,layer.GetName())
				tmp.SyncToDisk()
				tmp=None
				print("Content-type: application/gml+xml\n\n")
				with open(tmpfile[1],"r") as t:
					for g in t.readlines():
						print(g,end='')


if __name__ == "__main__":
	main()
